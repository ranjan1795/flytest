import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material'
import { LoginComponent } from '../login/login.component';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  images = [1, 2, 3].map(() => `https://picsum.photos/900/500?random&t=${Math.random()}`);
  
  
  constructor(public dialog: MatDialog) { 
  }
  ngOnInit() {
  }
openLoginForm(){
  this.dialog.open(LoginComponent, {width:'500px', height:'450px'});

}
}