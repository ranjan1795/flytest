import { Component, OnInit, ViewChild,Inject } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Feedback } from '../shared/feedback';
import { FeedbackService } from '../services/feedback.service';
import{flyInOut, visibility, hide, expand} from '../animations/app.animation';
import {visitSiblingRenderNodes} from "@angular/core/src/view/util";

@Component({
  selector: 'app-contact',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
  host: {
    '[@flyInOut]': 'true',
    'style': 'display: block;'
    },
    animations: [
      flyInOut(),
      visibility(),
      hide(),
      expand()
    ]
})
export class HomeComponent implements OnInit {
  

  //images = [1, 2, 3].map(() => `https://picsum.photos/900/500?random&t=${Math.random()}`);
  
  
  @ViewChild('fform') feedbackFormDirective;
  feedbackForm: FormGroup;
  feedback: Feedback;
  visibilityForm = 'shown';
  visibilitySpinner = 'hidden';
  

  formErrors={
    'telnum':'',
    'email':'',
    'destination':''
  };

  validationMessages ={
    'destination': {
      'required':      'Destination  is required.',
      'minlength':     'Destination must be at least 2 characters long.',
      'maxlength':     'Destination cannot be more than 50 characters long.'
    },
   'telnum': {
  'required':      'Tel. number is required.',
  'pattern':       'Tel. number must contain only numbers.',
  'minlength':  'Tel. number should be min. 10 number'
},
'email': {
  'required':      'Email is required.',
  'email':         'Email not in valid format.'
},
  };
  constructor( private fb: FormBuilder, private feedBackService: FeedbackService, @Inject('BaseURL') private BaseURL) {
    this.createForm();
 }

  ngOnInit() {
  }
createForm(){
  this.feedbackForm=this.fb.group({
    email: ['', [Validators.required, Validators.email]],
    telnum: ['', [Validators.required, Validators.pattern, Validators.minLength(10), Validators.maxLength(10) ]],
   destination: ['', [Validators.required, Validators.minLength(3), Validators.maxLength(50)]],
    });

  this.feedbackForm.valueChanges
  .subscribe(data => this.onValueChanged(data));

this.onValueChanged(); // (re)set validation messages now

}

  onValueChanged(data?: any) {
    if (!this.feedbackForm) { return; }
    const form = this.feedbackForm;
    for (const field in this.formErrors) {
      if (this.formErrors.hasOwnProperty(field)) {
        // clear previous error message (if any)
        this.formErrors[field] = '';
        const control = form.get(field);
        if (control && control.dirty && !control.valid) {
          const messages = this.validationMessages[field];
          for (const key in control.errors) {
            if (control.errors.hasOwnProperty(key)) {
              this.formErrors[field] += messages[key] + ' ';
            }
          }
        }
      }
    }
  }

  onSubmit() {
    this.visibilityForm = 'hidden';
    this.visibilitySpinner = 'shown';
    this.feedBackService.submitFeedback(this.feedbackForm.value)
      .subscribe(feedback => {
        this.visibilitySpinner = 'hidden';
        this.feedback = feedback;
         setTimeout(func =>{
          this.feedback = null;
          this.visibilityForm = 'shown';
          }, 5000); 

      });
  
  this.feedbackForm.reset({
    email: '',
    telnum:'',
   destination:''
    });

  this.feedbackFormDirective.resetForm();
}


}
